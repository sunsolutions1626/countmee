
/*
Title : OrderStatus Class
Purpose: Commonly used class to get order status
Created By : Kalpesh Khandla
Created On : N/A
Last Edited By : 3 Feb 2022
*/


class OrderStatus {
  static const int ORDER_COMPLETED = 7;
  static const int ORDER_REJECTED = 6;
  static const int ORDER_DELIVERED = 5;
  static const int ORDER_CANCELLED = 4;
  static const int ORDER_DISPATCH = 3;
  static const int ORDER_PROCESSING = 2;
  static const int ORDER_CREATED = 1;
}
